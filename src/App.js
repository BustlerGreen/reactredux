import logo from './logo.svg';
import './App.css';
import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './components/home';
import Login from './components/login';
import Register from './components/register';
import NotFound from './components/notfound';
import { Provider } from 'react-redux';
import store from './Redux/store';


function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element = {<Home/>}/>
          <Route path="/login" element= {<Login />} />
          <Route path="/register" element = {<Register/>} />
          <Route path="/NotFound" element = {<NotFound />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  )
}

export default App;
