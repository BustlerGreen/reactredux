import { Box } from "@mui/system";
import React from "react";
import { Button, Grid, Paper, Typography } from "@mui/material"; 
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { logoutUser } from "../Redux/actions";


export default function Home() {
    const dispath =useDispatch();
    const loggedUser = useSelector(state=> state.userMgt.loggedUserName);
    const navigate = useNavigate();
    return(
        <React.Fragment>
            <Grid container direction="column" justifyItems="center">
                <Paper margin="40px" >
                    <Typography backgroundColor="magenta" textAlign="center" variant="h3">This is a home page for otus react-redux hw</Typography>
                </Paper>
                <Grid container justifyContent="start" >
                <Button onClick={()=>navigate("/notFound")}>Search for black cat</Button>
                </Grid>
                <Grid container justifyContent="end">
                    {loggedUser === '' && <Button onClick={()=>navigate("/login")}>Login</Button>}
                    {loggedUser !== '' && <Button onClick={()=>{dispath(logoutUser())}}>{loggedUser} - Logout</Button>}
                    <Button onClick={()=>navigate("/register")}>Register</Button>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}