import React from "react";
import { Button, Grid, Typography } from "@mui/material";
import { useNavigate } from "react-router";

export default function NotFound() {
    const navigate = useNavigate();
    return(
        <React.Fragment>
            <Grid container justifyItems="center" direction="column" alignItems="center" >
                <Grid item xs={6} marginTop="40px">
                    <Typography backgroundColor="LightGray" textAlign="center" variant="h3">we did our best, </Typography>
                    <Typography backgroundColor="LightGray" textAlign="center" variant="h5">but didn't find that you were searched for</Typography>
                </Grid>
                <Grid item marginTop="20px">
                    <Button variant="contained" onClick={()=>navigate("/")}>Back to home</Button>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}