import { Grid, Container, Typography, TextField, Button } from "@mui/material"
import React, { useRef, useState } from "react"
import { useNavigate } from "react-router"
import { useDispatch, useSelector } from "react-redux";
import { addUser } from "../Redux/actions";
import {v4} from "uuid";


export default function Register() {
    const navigate = useNavigate();
    const [state, setState] = useState({
        login: '',
        psw: '',
        email: '',
    })
    const dispath = useDispatch();
    const users = useSelector(state=> state.userMgt.users)

    const OnOk = ()=>{
        let user = users.find(item=>  item.user.login === state.login);
        if(user) { alert(`пользоваетель ${state.login} уже существует`); return}
        dispath(addUser({
            id: v4(),
            login: state.login,
            psw: state.psw,
            email: state.email,
        }));
        navigate('/');
    }
    const OnCancel = ()=>{
        navigate("/");
    }

    return(
        <React.Fragment>
            <Grid  container direction="column" justifyContent="space-round" alignItems="center" >
                    <Grid item marginTop="20px">
                        <Typography variant="h3">new user registration</Typography>
                    </Grid>
                    <Grid item marginTop="20px"  >
                        <TextField id="user_name" label="user name" required color="secondary" value={state.login}
                            onChange={(e)=>setState((oldstate) => {return{...oldstate, login: e.target.value,}})}/>
                        <br/>
                        <TextField id="user_email" label="email " required color="secondary" value={state.email}
                            onChange={(e)=>setState((oldstate) => {return{...oldstate, email: e.target.value,}})}/>
                        <br/>
                        <TextField id="user_psw" label="password" required color="secondary" type="password" value={state.psw}
                            onChange={(e)=>setState((oldstate) => {return{...oldstate, psw: e.target.value,}})}/>
                    </Grid>
                    <Grid container justifyContent="center" marginTop="10px" >
                        <Button margin-right="20px" variant="contained" onClick={OnOk}>Register</Button>
                        <Button variant="contained" onClick={OnCancel}>Cancel</Button>
                    </Grid>
                </Grid>
        </React.Fragment>
    )
}