import { Grid, Container, Typography, TextField, Button } from "@mui/material"
import React, { useRef, useState } from "react"
import { useNavigate } from "react-router"
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../Redux/actions";

export default function Login() {
    const navigate = useNavigate();
    const [state, setState] = useState({
        login: '',
        psw: '',
    })
    const dispath = useDispatch();
    const users = useSelector(state=> state.userMgt.users)
    
    const OnOk = ()=>{
        const user = users.find(item=> item.user.login === state.login & item.user.psw === state.psw);
        if(!user) { alert('неверный логин/пароль'); return}
        dispath(loginUser({id: user.user.id, name: user.user.login}));
        navigate('/');
    }
    const OnCancel = ()=>{
        navigate("/");
    }
    return (
        <React.Fragment>
                <Grid  container direction="column" justifyContent="space-round" alignItems="center" >
                    <Grid item marginTop="20px">
                        <Typography variant="body1">Identify yourself</Typography>
                    </Grid>
                    <Grid item marginTop="20px"  >
                        <TextField label="user name" required color="secondary" value={state.login} 
                            onChange={(e)=>setState((oldstate) => {return{...oldstate, login: e.target.value,}})}/>
                        <br/>
                        <TextField id="user_psw" label="password" required color="secondary" type="password" value={state.psw}
                            onChange={(e)=>setState((oldstate) => {return{...oldstate, psw: e.target.value,}})}/>
                    </Grid>
                    <Grid container justifyContent="center" marginTop="10px" >
                        <Button margin-right="20px" variant="contained" onClick={OnOk}>Login</Button>
                        <Button variant="contained" onClick={OnCancel}>Cancel</Button>
                    </Grid>
                </Grid>

        </React.Fragment>
    )
}