
const ADD_USER = "ADD_USER";
const USER_LOGOUT = "USER_LOGOUT";
const USER_LOGIN = "USER_LOGIN";

const addUser = function(user){
    return{
        type: ADD_USER,
        payload: {user},
    }
}

const logoutUser = function(){
    return {
        type: USER_LOGOUT,
        payload: {},
    }
}

const loginUser = function(user) {
    return {
        type: USER_LOGIN,
        payload: {user},
    }
}

export {ADD_USER, addUser, USER_LOGIN, USER_LOGOUT, loginUser, logoutUser};
