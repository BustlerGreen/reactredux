import { ADD_USER, USER_LOGOUT, USER_LOGIN } from "./actions";

export const userData = {
    id: '',
    login: '',
    psw: '',
    email:'',
}
export const initUserData = {
    userMgt: {
        users: [], // userData
        loggedUser: '',
        loggedUserName: '',
    }
 }

export default function reducer(state= initUserData,action){
    
    switch(action.type){
        case ADD_USER : {
            let nusers = state.userMgt.users.slice();
            nusers.splice(nusers.length-1, 0, action.payload);
            return {
                ...state, userMgt :{
                    ...state.userMgt, users : nusers
                }
            }
        }
        case USER_LOGOUT : {
            return {
                ...state, userMgt: {
                    ...state.userMgt, loggedUser :'', loggedUserName: '',
                }
            }
        }

        case USER_LOGIN:{
            return {
                ...state, userMgt :{
                    ...state.userMgt, loggedUser: action.payload.user.id, loggedUserName: action.payload.user.name
                }
            }
        }
    }
    return state;
    
}